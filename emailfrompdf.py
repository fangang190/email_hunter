#!/usr/bin/python
# -*- coding:utf-8 -*-

__author__ = 'fangang'
import logging
import os
import re
import shutil
import subprocess
import sys
import textwrap
import xlsxwriter

from termcolor import colored

logging.basicConfig(filename='runtest.log', level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

PDFTOTEXT = ""

# a python version of which program in xnix
def which(program):
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    def ext_candidates(fpath):
        yield fpath
        for ext in os.environ.get("PATHEXT", "").split(os.pathsep):
            yield fpath + ext

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            for candidate in ext_candidates(exe_file):
                if is_exe(candidate):
                    return candidate

    return None

# xx@xx.com
s = r'([\w]+[.|\w])+@([\w|-]+[.])*\w+'
email_regex = re.compile(s)  # domain

# {xx,yy,zz}@xx.com
group_email_regex = re.compile(r'\{[ ]{0,2}(\w+[.|\w|,| )]{0,2})+\}@([\w|-]+[.])*\w+')

# get the pdf first page text
def pdf_firstpage_text(pdf_file):
    # the command pdftotext is much faster than slate package.

    temp_dir = './temp_hunter'
    temp_text = temp_dir + os.sep + "temp.txt"
    try:
        os.mkdir(temp_dir)
    except:
        pass

    commands = [PDFTOTEXT, pdf_file, '-f', '1', '-l', '1', temp_text]
    subprocess.call(commands, shell=False)

    # get the content of a file
    content = ''
    with open(temp_text) as f:
        content = f.read()

    try:
        shutil.rmtree(temp_dir)
    except:
        pass
    print content
    return content


def group_to_emails(group_email):
    # {xx,yy,zz}@xx.com like emails to xx@xx.com yy@xx.com array
    print group_email
    names_str, domain = group_email.split('@')
    names_str = re.sub('[\s+]', '', names_str)

    names_str = names_str.strip('{')
    names_str = names_str.strip('}')
    names = names_str.split(',')
    return [n + '@' + domain for n in names]


def get_email_from_file(file):
    # convert pdf to text
    # using slate library


    emails = []
    with open(file) as f:
        first_page = pdf_firstpage_text(file)
        # search email in the text.
        # Regular email addresses.
        index = 0
        while True:
            matched = email_regex.search(first_page, index)
            if matched is None:
                break
            index = matched.end()
            emails.append(matched.group())

        index = 0
        while True:
            matched = group_email_regex.search(first_page, index)
            if matched is None:
                break
            index = matched.end()
            group_emails = group_to_emails(matched.group())
            emails.extend(group_emails)

    return file, emails


output_file = "output.xlsx"

def runFolder(folder):
    logging.info("runOnFolder  " + folder + "...")

    results = {}

    for root, dirs, files in os.walk(folder):
        for file in files:

            # check if there is a MakeFile under that dir
            dir = os.path.abspath(root)

            if file.endswith(".pdf"):
                logging.info("Checking " + file + "...")
                file, emails = get_email_from_file(dir + os.sep + file)
                results[file] = emails

    user = "Username"
    prefix = user + ": "
    preferredWidth = 70
    wrapper = textwrap.TextWrapper(initial_indent=prefix, width=preferredWidth,
                                   subsequent_indent=' ' * len(prefix))

    logging.info("------------------------------------------------------------------------------------------")
    for filename in results:
        emails = results[filename]
        logging.info('FileName: %s', filename)
        [logging.info('\t\t%s', x) for x in emails]

    logging.info("------------------------------------------------------------------------------------------")

    # write it to a excel spreadsheet
    emails = []
    for filename in results:
        emails.extend(results[filename])

    # remove duplicate entries.
    emails = set(emails)

    global output_file
    write_to_excel(emails, output_file)
    return 0





def write_to_excel(emails, output_file):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(output_file)
    worksheet = workbook.add_worksheet()

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    # Iterate over the data and write it out row by row.
    for email in (emails):
        worksheet.write(row, col,     email)
        row += 1

    workbook.close()



def printHelp():

    print "./emailfrompdf.py %s %s" % (colored("pdf_path", "red"), colored("xx.xlsx", "green"))



def main():

    global output_file

    if which(PDFTOTEXT) == None:
        print "Cannot find pdftotext in current system. "
        exit(1)

    # Parse options from cmd line
    folder = ""
    if len(sys.argv) != 3:
        printHelp()
    else:
        folder = sys.argv[1]
        output_file = sys.argv[2]

    ret = runFolder(folder)
    return sys.exit(ret)


# ================================================
# Module starting point
# ================================================
try:
    PDFTOTEXT = os.environ['PDFTOTEXT']
except KeyError:
    # Find pp-check in global path
    PDFTOTEXT = "pdftotext"

if __name__ == '__main__':
    main()
